package generalEntities;

public class Order {
	public String id;
	
	public String beginPoint;
	public String endPoint;
	public OrderStatus status;
	public Driver driver;
	public String orderTime;
	public boolean withChild;
	public String comment;
	
	public int approximateCost;
	public int realCost;
	
	public String clientId;

}
