package driver.transport;

import generalEntities.Response;

public class ChangeEmploymentResponse extends Response{
	
	public ChangeEmploymentResponse() {
		comment = "Change Employment Complete";
	}

}
