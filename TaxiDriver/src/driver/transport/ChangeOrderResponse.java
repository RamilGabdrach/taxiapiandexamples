package driver.transport;

import generalEntities.Response;

public class ChangeOrderResponse extends Response{
	public ChangeOrderResponse() {
		comment = "Change Order Status Complete";
	}
}
