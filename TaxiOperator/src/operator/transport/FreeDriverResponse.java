package operator.transport;

import generalEntities.Driver;
import generalEntities.Response;

import java.util.ArrayList;

public class FreeDriverResponse extends Response{
	
	public ArrayList<Driver> drivers;
	
	public FreeDriverResponse(ArrayList<Driver> dr) {
		drivers = dr;
		comment = "Free Driver Response Complete";
	}

}
