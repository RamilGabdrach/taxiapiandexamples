package generalEntities;

import java.io.DataOutputStream;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Request {
	public String comment;
	public void request(DataOutputStream out) throws IOException
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		out.writeUTF(json);
		out.flush();
		System.out.println("request   "+json+"   send");
	}
}
