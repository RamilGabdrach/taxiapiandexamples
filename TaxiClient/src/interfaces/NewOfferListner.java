package interfaces;

import generalEntities.Driver;
import generalEntities.Order;

public interface NewOfferListner {

	public void OnNewOffer(Driver driver, Order order);
}
