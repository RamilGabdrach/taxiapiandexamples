package main;

import generalEntities.Order;
import generalEntities.OrderStatus;
import generalEntities.Response;
import generalEntities.StatusChangeResponse;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;













import com.google.gson.Gson;

import driver.transport.ChangeEmploymentRequest;
import driver.transport.ChangeEmploymentResponse;
import driver.transport.ChangeLocalRequest;
import driver.transport.ChangeLocalResponse;
import driver.transport.ChangeOrderRequest;
import driver.transport.ChangeOrderResponse;
import driver.transport.InitRequest;
import driver.transport.InitResponse;
import driver.transport.OfferOrderResponse;

public class DriverThread extends Thread{
	private int client_num;
	int serverPort = 1988; // ����� ����������� ����� ������� ���� � �������� ������������� ������.
    String address = "54.200.79.181";
	
    boolean isInit = false;
    boolean waitAnswer = false;
    
	public DriverThread(int i)
	{
		client_num = i;
	}
	
	public void run() {
		try {
           InetAddress ipAddress = InetAddress.getByName(address); // ������� ������ ������� ���������� ������������� IP-�����.
           System.out.println("Any of you heard of a socket with IP address " + address + " and port " + serverPort + "?");
           Socket socket = new Socket(ipAddress, serverPort); // ������� ����� ��������� IP-����� � ���� �������.
           System.out.println("Yes! I just got hold of the program.");

           // ����� ������� � �������� ������ ������, ������ ����� �������� � �������� ������ ��������. 
           InputStream sin = socket.getInputStream();
           OutputStream sout = socket.getOutputStream();

           // ������������ ������ � ������ ���, ���� ����� ������������ ��������� ���������.
           DataInputStream in = new DataInputStream(sin);
           final DataOutputStream out = new DataOutputStream(sout);

           // ������� ����� ��� ������ � ����������.
           String line = null;
           System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
           System.out.println();
           
           
           
           
           final Timer t = new Timer();
		    TimerTask time = new TimerTask()
		    { 
			    public void run()
			    {
			    	try {
						ChangeLocal(out);
			    		//restoreOrder(out);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	t.cancel();
			    	t.purge();
			    }
		    }; 
		    t.scheduleAtFixedRate(time, 10000, 60000); 

           while (true) {
               
        	   if(isInit)
        	   {
        		   line = in.readUTF(); // ���� ���� ������ ������� ������ ������.
        		   Response response = (Response) new Gson().fromJson(line, Response.class);
        		   
        		   if(waitAnswer)
        		   {
	        		   System.out.println("Server answered to me: "+ line);

	        		   if(response.comment.equals("Local Change Complete"))
	        		   {
		        		   ChangeLocalResponse localResponse;
		        		   if((localResponse = (ChangeLocalResponse) new Gson().fromJson(line, ChangeLocalResponse.class)) !=null)
		        		   {
		        			   System.out.println("Local Change Complete");
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		   if(response.comment.equals("Change Order Status Complete"))
	        		   {
		        		   ChangeOrderResponse orderResponse;
		        		   if((orderResponse= (ChangeOrderResponse) new Gson().fromJson(line, ChangeOrderResponse.class)) !=null)
		        		   {
		        			   System.out.println("Change Order Status Complete");
		        			  
		        			   
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		   if(response.comment.equals("Change Employment Complete"))
	        		   {
		        		   ChangeEmploymentResponse emplResponse;
		        		   if((emplResponse = (ChangeEmploymentResponse) new Gson().fromJson(line, ChangeEmploymentResponse.class)) !=null)
		        		   {
		        			   System.out.println("Change Employment Complete");
		        			  
		        			   
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		  
	        		   
	        		   if(waitAnswer)
	        		   {
	        			   System.out.println("Response is not recognit");
	        			   Error e = (Error) new Gson().fromJson(line, Error.class);
	        		   }
	        		   
	        		   
	        		   
        		   }
        		   else
        		   {
        			   System.out.println("Server send to me question: "+ line);
        			   
        			   if(response.comment.equals("Offer Order Response"))
	        		   {
	        			   OfferOrderResponse offerResponse;
		        		   if((offerResponse = (OfferOrderResponse) new Gson().fromJson(line, OfferOrderResponse.class)) !=null)
		        		   {
		        			   System.out.println("Offer Order Response");
		        			   Order order = offerResponse.order;
		        			   waitAnswer = false;
		        		   }
	        		   }
        		   }
        	   }
        	   else
        	   {
        			   init(in, out);
        	   }
        	  
           }
       } catch (Exception x) {
           x.printStackTrace();
       }
		
	}
	
	private void init(DataInputStream in, DataOutputStream out) throws IOException {
		InitRequest request = new InitRequest("user","1234");
		request.request(out);
		
		String line = in.readUTF();
		System.out.println("Server answered to me: "+ line);
		
		InitResponse initResponse;
		if((initResponse = (InitResponse) new Gson().fromJson(line, InitResponse.class)) != null)
		{
			isInit = true;
		}
	}
	
	private void  ChangeLocal(DataOutputStream out) throws IOException {
		ChangeLocalRequest request = new ChangeLocalRequest("51.2", "21.1");
    	
    	request.request(out);
    	waitAnswer = true;
		
	}
	
	private void ChangeOrderStatus(DataOutputStream out, Order o) throws IOException {
		ChangeOrderRequest request = new ChangeOrderRequest(o);   	
		
		request.request(out);
		waitAnswer = true;
		
	}
	
	private void ChangeDriverStatus(DataOutputStream out) throws IOException {
		ChangeEmploymentRequest request = new ChangeEmploymentRequest(true);   	
		
		request.request(out);
		waitAnswer = true;
		
	}
	
	
	
}
