package interfaces;

import generalEntities.Order;

public interface StatusChangeListner {
	
	public void onStatusChange(Order order);
}
