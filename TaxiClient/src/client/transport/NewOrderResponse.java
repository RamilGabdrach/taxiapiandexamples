package client.transport;

import generalEntities.Response;

public class NewOrderResponse extends Response{
	
	public NewOrderResponse()
	{
		comment = "New Order Registration complete";
	}
}
