package driver.transport;

import generalEntities.Request;

public class InitRequest extends Request{
	public String password;
	public String login;
	
	public InitRequest(String log, String pass) {
		login = log;
		password = pass;
		
		comment = "Inizialization";
	}
}
