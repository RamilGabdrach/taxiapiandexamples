package client.transport;

import generalEntities.Response;

public class InitResponse extends Response{
	public boolean isAuthorized;
	
	
	public InitResponse(boolean authorized)
	{
		isAuthorized = authorized;
		
		if(isAuthorized)
		{
			comment = "User authorized";
		}
		else
		{
			comment = "User is'n authorized. Need registration";
		}
		
	}
}
