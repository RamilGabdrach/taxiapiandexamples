package driver.transport;

import generalEntities.Request;

public class ChangeEmploymentRequest extends Request{
	public boolean free;
	
	public ChangeEmploymentRequest(boolean isFree)
	{
		free = isFree;
		comment = "Change Employment Request";
	}
}
