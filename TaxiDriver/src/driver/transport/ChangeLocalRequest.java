package driver.transport;

import generalEntities.Request;

public class ChangeLocalRequest extends Request{
	public String latitude;
	public String longitude;
	
	public ChangeLocalRequest(String lat,String lon) {
		latitude= lat;
		longitude = lon;
		comment = "Change Local Request";
	}
}
