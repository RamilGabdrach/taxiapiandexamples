package operator.transport;

import generalEntities.Response;


public class TryDriverToOrderResponse extends Response {
	
	public TryDriverToOrderResponse() {
		comment = "Try Driver Complete";
	}
}
