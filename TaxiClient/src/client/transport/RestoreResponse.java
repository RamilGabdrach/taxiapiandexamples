package client.transport;

import generalEntities.Order;
import generalEntities.Response;

public class RestoreResponse extends Response {
	public Order order;
	
	public RestoreResponse(Order o)
	{
		order = o;
		comment = "Restore Response Complete";
	}
}
