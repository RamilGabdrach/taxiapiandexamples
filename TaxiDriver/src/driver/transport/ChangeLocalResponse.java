package driver.transport;

import generalEntities.Response;

public class ChangeLocalResponse extends Response{
	public ChangeLocalResponse() {
		comment = "Local Change Complete";
	}
}
