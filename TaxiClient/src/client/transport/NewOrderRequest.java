package client.transport;

import generalEntities.Order;
import generalEntities.Request;

public class NewOrderRequest extends Request{
	public Order order;
	
	public NewOrderRequest() {
		comment = "New Order Request";
	}
}
