package main;

import generalEntities.Driver;
import generalEntities.Order;
import generalEntities.OrderStatus;
import generalEntities.Response;
import generalEntities.StatusChangeResponse;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import operator.transport.FreeDriverRequest;
import operator.transport.FreeDriverResponse;
import operator.transport.InitRequest;
import operator.transport.InitResponse;
import operator.transport.TryDriverToOrderRequest;
import operator.transport.TryDriverToOrderResponse;

import com.google.gson.Gson;

public class OperatorThread extends Thread{
	private int client_num;
	int serverPort = 1998; // ����� ����������� ����� ������� ���� � �������� ������������� ������.
    String address = "54.200.79.181";
	
    boolean isInit = false;
    boolean waitAnswer = false;
    
	public OperatorThread(int i)
	{
		client_num = i;
	}
	
	public void run() {
		try {
           InetAddress ipAddress = InetAddress.getByName(address); // ������� ������ ������� ���������� ������������� IP-�����.
           System.out.println("Any of you heard of a socket with IP address " + address + " and port " + serverPort + "?");
           Socket socket = new Socket(ipAddress, serverPort); // ������� ����� ��������� IP-����� � ���� �������.
           System.out.println("Yes! I just got hold of the program.");

           // ����� ������� � �������� ������ ������, ������ ����� �������� � �������� ������ ��������. 
           InputStream sin = socket.getInputStream();
           OutputStream sout = socket.getOutputStream();

           // ������������ ������ � ������ ���, ���� ����� ������������ ��������� ���������.
           DataInputStream in = new DataInputStream(sin);
           final DataOutputStream out = new DataOutputStream(sout);

           // ������� ����� ��� ������ � ����������.
           String line = null;
           System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
           System.out.println();
           
           
           
           
           final Timer t = new Timer();
		    TimerTask time = new TimerTask()
		    { 
			    public void run()
			    {
			    	try {
			    		FreeDriver(out);
			    		//restoreOrder(out);
			    		
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	t.cancel();
			    	t.purge();
			    }
		    }; 
		    t.scheduleAtFixedRate(time, 10000, 60000); 

           while (true) {
               
        	   if(isInit)
        	   {
        		   line = in.readUTF(); // ���� ���� ������ ������� ������ ������.
        		   Response response = (Response) new Gson().fromJson(line, Response.class);
        		   
        		   if(waitAnswer)
        		   {
	        		   System.out.println("Server answered to me: "+ line);

	        		   if(response.comment.equals("Free Driver Response Complete"))
	        		   {
		        		   FreeDriverResponse freeResponse;
		        		   if((freeResponse = (FreeDriverResponse) new Gson().fromJson(line, FreeDriverResponse.class)) !=null)
		        		   {
		        			   System.out.println("Free Driver Response Complete");
		        			   //freeResponse.drivers.get(i)
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		   if(response.comment.equals("Try Driver Complete"))
	        		   {
		        		   TryDriverToOrderResponse tryResponse;
		        		   if((tryResponse= (TryDriverToOrderResponse) new Gson().fromJson(line, TryDriverToOrderResponse.class)) !=null)
		        		   {
		        			   System.out.println("Try Driver Complete");
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		
	        		   
	        		   if(waitAnswer)
	        		   {
	        			   System.out.println("Response is not recognit");
	        			   Error e = (Error) new Gson().fromJson(line, Error.class);
	        		   }
	        		   
	        		   
	        		   
        		   }
        		   else
        		   {
        			   System.out.println("Server send to me question: "+ line);
        			   
        			   if(response.comment.equals("Status Chanhe Response"))
	        		   {
	        			   StatusChangeResponse statusChangeResponse;
		        		   if((statusChangeResponse = (StatusChangeResponse) new Gson().fromJson(line, StatusChangeResponse.class)) !=null)
		        		   {
		        			   System.out.println("Status Change Response Complete");
		        			   Order order = statusChangeResponse.order;
		        			   waitAnswer = false;
		        		   }
	        		   }
        		   }
        	   }
        	   else
        	   {
        			   init(in, out);
        	   }
        	  
           }
       } catch (Exception x) {
           x.printStackTrace();
       }
		
	}
	
	private void init(DataInputStream in, DataOutputStream out) throws IOException {
		InitRequest request = new InitRequest("12");
		request.request(out);
		
		String line = in.readUTF();
		System.out.println("Server answered to me: "+ line);
		
		InitResponse initResponse;
		if((initResponse = (InitResponse) new Gson().fromJson(line, InitResponse.class)) != null)
		{
			isInit = true;			
		}
	}
	
	private void  FreeDriver(DataOutputStream out) throws IOException {
		FreeDriverRequest request = new FreeDriverRequest();
    	request.request(out);
    	waitAnswer = true;
		
	}
	
	private void TryDriverToOrder(DataOutputStream out, Order o, Driver d) throws IOException {
		
		TryDriverToOrderRequest request = new TryDriverToOrderRequest(d, o);   	
		request.request(out);
		waitAnswer = true;
		
	}
	
	
	
}
