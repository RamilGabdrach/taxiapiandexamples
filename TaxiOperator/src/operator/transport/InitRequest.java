package operator.transport;

import generalEntities.Request;

public class InitRequest extends Request{
	
	public String deviceId;
	public InitRequest(String id) {
		deviceId = id;
		comment = "Inizialization";
	}
	
}
