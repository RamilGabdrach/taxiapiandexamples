package client.transport;

import generalEntities.Request;

public class RestoreRequest extends Request{
	
	public RestoreRequest() {
		comment = "Restore Order Request";
	}
}
