package driver.transport;

import generalEntities.Response;

public class InitResponse extends Response{
	public InitResponse() {
		comment = "Init complete";
	}
}
