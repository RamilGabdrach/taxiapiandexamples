package main;

import generalEntities.Order;
import generalEntities.OrderStatus;
import generalEntities.Response;
import generalEntities.StatusChangeResponse;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import client.transport.InitRequest;
import client.transport.InitResponse;
import client.transport.NewOrderRequest;
import client.transport.NewOrderResponse;
import client.transport.RegRequest;
import client.transport.RegResponse;
import client.transport.RestoreRequest;
import client.transport.RestoreResponse;

import com.google.gson.Gson;

public class ClientThread extends Thread{
	private int client_num;
	int serverPort = 1978; // ����� ����������� ����� ������� ���� � �������� ������������� ������.
    String address = "54.200.79.181";
	
    boolean isInit = false;
    boolean waitAnswer = false;
    
	public ClientThread(int i)
	{
		client_num = i;
	}
	
	public void run() {
		try {
           InetAddress ipAddress = InetAddress.getByName(address); // ������� ������ ������� ���������� ������������� IP-�����.
           System.out.println("Any of you heard of a socket with IP address " + address + " and port " + serverPort + "?");
           Socket socket = new Socket(ipAddress, serverPort); // ������� ����� ��������� IP-����� � ���� �������.
           System.out.println("Yes! I just got hold of the program.");

           // ����� ������� � �������� ������ ������, ������ ����� �������� � �������� ������ ��������. 
           InputStream sin = socket.getInputStream();
           OutputStream sout = socket.getOutputStream();

           // ������������ ������ � ������ ���, ���� ����� ������������ ��������� ���������.
           DataInputStream in = new DataInputStream(sin);
           final DataOutputStream out = new DataOutputStream(sout);

           // ������� ����� ��� ������ � ����������.
           String line = null;
           System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
           System.out.println();
           
           
           
           
           final Timer t = new Timer();
		    TimerTask time = new TimerTask()
		    { 
			    public void run()
			    {
			    	try {
						newOrder(out);
			    		//restoreOrder(out);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	t.cancel();
			    	t.purge();
			    }
		    }; 
		    t.scheduleAtFixedRate(time, 10000, 60000); 

           while (true) {
               
        	   if(isInit)
        	   {
        		   line = in.readUTF(); // ���� ���� ������ ������� ������ ������.
        		   Response response = (Response) new Gson().fromJson(line, Response.class);
        		   
        		   if(waitAnswer)
        		   {
	        		   System.out.println("Server answered to me: "+ line);

	        		   if(response.comment.equals("Registration complete"))
	        		   {
		        		   RegResponse regResponse;
		        		   if((regResponse = (RegResponse) new Gson().fromJson(line, RegResponse.class)) !=null)
		        		   {
		        			   System.out.println("Reg Complete");
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		   if(response.comment.equals("Restore Response Complete"))
	        		   {
		        		   RestoreResponse restoreResponse;
		        		   if((restoreResponse= (RestoreResponse) new Gson().fromJson(line, RestoreResponse.class)) !=null)
		        		   {
		        			   System.out.println("Restore Complete");
		        			   
		        			   Order order = restoreResponse.order; // cant be null!
		        			   
		        			   waitAnswer = false;
		        		   }
	        		   }
	        		   
	        		   
	        		   if(response.comment.equals("New Order Registration complete"))
	        		   {
		        		   NewOrderResponse newOrderResponse;
		        		   if((newOrderResponse= (NewOrderResponse) new Gson().fromJson(line, NewOrderResponse.class)) !=null)
		        		   {
		        			   System.out.println("New Order Response Complete");
		        			   waitAnswer = false;
		        		   }
	        		   }   
	        		   
	        		   if(waitAnswer)
	        		   {
	        			   System.out.println("Response is not recognit");
	        			   Error e = (Error) new Gson().fromJson(line, Error.class);
	        		   }
	        		   
	        		   
	        		   
        		   }
        		   else
        		   {
        			   System.out.println("Server send to me question: "+ line);
        			   
        			   if(response.comment.equals("Status Chanhe Response"))
	        		   {
	        			   StatusChangeResponse statusChangeResponse;
		        		   if((statusChangeResponse = (StatusChangeResponse) new Gson().fromJson(line, StatusChangeResponse.class)) !=null)
		        		   {
		        			   System.out.println("Status Change Response Complete");
		        			   Order order = statusChangeResponse.order;
		        			   waitAnswer = false;
		        		   }
	        		   }
        		   }
        	   }
        	   else
        	   {
        			   init(in, out);
        	   }
        	  
           }
       } catch (Exception x) {
           x.printStackTrace();
       }
		
	}

	private void registrate(DataOutputStream out) throws IOException {
		 RegRequest request = new RegRequest();
		 request.Name = "Ramil";
		 request.PhoneNubmer = "89656015377";
		 request.request(out);
		 waitAnswer = true;
	}
	
	private void init(DataInputStream in, DataOutputStream out) throws IOException {
		InitRequest request = new InitRequest();
		request.deviceId = "517";
		request.request(out);
		
		String line = in.readUTF();
		System.out.println("Server answered to me: "+ line);
		
		InitResponse initResponse;
		if((initResponse = (InitResponse) new Gson().fromJson(line, InitResponse.class)) != null)
		{
			isInit = true;
			if(!initResponse.isAuthorized)
				registrate(out);				
		}
	}
	
	private void  newOrder(DataOutputStream out) throws IOException {
		NewOrderRequest request = new NewOrderRequest();
		
		Order order = new Order();
    	order.beginPoint = "Gagarin str. 43";
    	order.endPoint = "Univer str.  32";
    	order.status = OrderStatus.NEW;
    	order.orderTime = new Date().toString();
    	order.withChild = false;
    	order.comment = "��������� ����� ���!";
    	order.approximateCost = 100;
    	
    	request.order = order;
    	
    	request.request(out);
    	waitAnswer = true;
		
	}
	
	private void restoreOrder(DataOutputStream out) throws IOException {
		RestoreRequest request = new RestoreRequest();   	
		
		request.request(out);
		waitAnswer = true;
		
	}
	
	
	
}
