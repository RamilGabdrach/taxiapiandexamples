package driver.transport;

import generalEntities.Order;
import generalEntities.Request;

public class ChangeOrderRequest extends Request{
	public Order order;
	
	public ChangeOrderRequest(Order o) {
		order = o;
		comment = "Change Order Request";
	}
}
