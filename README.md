1. Скачайте репозиторий перейдя по ссылке: [download](https://bitbucket.org/RamilGabdrach/taxiapiandexamples/downloads)
2. Распакуйте архив
3. Запустите Eclipse
4. На правую кнопку->import->General->Existing Project Into Workspace
5. Выбрать папку, в которую распаковали архив
6. Выбрать все те проекты, которые Вам интересны.
7. Нажать финиш.
