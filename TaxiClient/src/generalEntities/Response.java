package generalEntities;

import java.io.DataOutputStream;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Response {
	
	public String comment;
	
	public void response(DataOutputStream out) throws IOException
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		out.writeUTF(json);
		
		System.out.println("response");
	}

}
