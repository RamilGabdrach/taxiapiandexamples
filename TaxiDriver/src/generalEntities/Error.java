package generalEntities;

import java.io.DataOutputStream;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Error {
	public String message;
	
	/**
	 * Конструктор
	 * @param mes
	 */
	
	public Error(String mes)
	{
		message = mes;
	}
	
	public void response(DataOutputStream out) throws IOException
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		out.writeUTF(json);
	}
}

