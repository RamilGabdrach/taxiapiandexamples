package generalEntities;

public enum OrderStatus {
	NEW(0), SEARCHDRIVER(1), DRIVERDECLINCE(2), DEFINEDRIVER(3), WAITCLIENT(4), EXECUTION(5), COMPLETE(6);
	
	
	private final int value;
    private OrderStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
