package client.transport;

import generalEntities.Response;

public class RegResponse extends Response{
	
	public RegResponse()
	{
		comment = "Registration complete";
	}
}
