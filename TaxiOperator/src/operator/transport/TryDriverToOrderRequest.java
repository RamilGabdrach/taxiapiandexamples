package operator.transport;

import generalEntities.Driver;
import generalEntities.Order;
import generalEntities.Request;

public class TryDriverToOrderRequest extends Request{
	public Driver driver;
	public Order order;
	
	public TryDriverToOrderRequest(Driver d, Order o) {
		driver = d;
		order = o;
		comment = "Try Driver To Order Request";
	}
}
