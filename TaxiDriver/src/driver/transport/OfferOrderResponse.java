package driver.transport;

import generalEntities.Order;
import generalEntities.Response;

public class OfferOrderResponse extends Response{
	public Order order;
	public OfferOrderResponse(Order or) {
		order = or;
		comment = "Offer Order Response";
	}
}
